
import express, {response} from "express";
import {AnyError, MongoError} from "mongodb";
import {MongoDB} from "./MongoDB";


export class ApiAnswer {
    static response=null
    static mongoDB:MongoDB
    static setMongoDB(mongoDB){
        this.mongoDB=mongoDB;
    }
    static closeConnection(){
        this.mongoDB?.closeConnect();
    }
    static returnTrueEmptyAnswer(response:any) {
        this.closeConnection();
        return response.send({
            status: true,
            message: '',
            result: null
        });
    }

    static returnNotSuchAnswer(response:any) {
        this.closeConnection();
        return response.send({
            status: false,
            message: "No Such Container",
            result: null
        });

    }

    static returnAnswerWithValue(res: any[],response) {
        this.closeConnection();
        console.log({data:res})
        return response.send({
            status: true,
            message: "",
            result: {data:res}
        });
    }   

    static catchErrorAnswer(p: Promise<void | MongoError>,response) {
       p.catch(rej => {
           this.closeConnection();
           ApiAnswer.returnNotSuchAnswer(response)
       });
    }

}