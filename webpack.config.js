module.exports = {
    //...
    watch: true,
    watchOptions: {
        poll: 1000, // Check for changes every second
    },
};